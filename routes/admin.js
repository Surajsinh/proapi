const router = require("express").Router();
const { login } = require("../controller/admin/login");
const { register } = require("../controller/admin/register");
const { admin } = require("../controller/admin/admin");
const { updateAdmin } = require("../controller/admin/updateAdmin");
const { deleteAdmin } = require("../controller/admin/deleteAdmin");
const { verifyTokenAdmin} = require("../controller/verifyToken");

router.get("/", (req, res) => {
    res.send(`Admin page`);
});

router.post("/register", register);
router.post("/login", login);
router.get("/admin/:id", verifyTokenAdmin, admin);
router.put("/admin/:id",verifyTokenAdmin , updateAdmin);
router.delete("/admin/:id",verifyTokenAdmin, deleteAdmin);

module.exports = router;
