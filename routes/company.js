const router = require("express").Router();
const { companyLogin } = require("../controller/company/CompanyLogin");
const { companyRegister } = require("../controller/company/CompanyRegister");
const { allCompany } = require("../controller/company/allCompany");
const { company } = require("../controller/company/Company");
const { updateCompany } = require("../controller/company/updateCompany");
const { deleteCompany } = require("../controller/company/deleteCompany");
const { verifyTokenAuth } = require("../controller/verifyToken");

router.get("/", (req, res) => {
    res.send(`Users page`);
});

router.post("/CompanyRegister", companyRegister);
router.post("/CompanyLogin", companyLogin);
router.get("/AllCompany",verifyTokenAuth, allCompany);
router.get("/Company/:id",verifyTokenAuth, company);
router.put("/Company/:id",verifyTokenAuth, updateCompany);
router.delete("/Company/:id",verifyTokenAuth, deleteCompany);

module.exports = router;
