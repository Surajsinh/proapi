const router = require("express").Router();
const { login } = require("../controller/user/login");
const { register } = require("../controller/user/register");
const { allUser } = require("../controller/user/allUser");
const { user } = require("../controller/user/User");
const { updateUser } = require("../controller/user/updateUser");
const { deleteUser } = require("../controller/user/deleteUser");
const { verifyTokenAuth } = require("../controller/verifyToken");


router.get("/", (req, res) => {
    res.send(`Users page`);
});

router.post("/Register", register);
router.post("/Login", login);
router.get("/AllUser",verifyTokenAuth, allUser);
router.get("/User/:id",verifyTokenAuth, user);
router.put("/User/:id",verifyTokenAuth, updateUser);
router.delete("/User/:id",verifyTokenAuth, deleteUser);

module.exports = router;

