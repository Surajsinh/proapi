const Admin = require("../../modules/admin");

const deleteAdmin = async (req, res) => {
    try {
        await Admin.findByIdAndDelete(req.params.id);
        res.status(200).send(`Admin Deleted`);
    } catch (err) {
        res.status(500).json(err);
    }
};

module.exports = { deleteAdmin };