const Admin = require("../../modules/admin");

const admin = async (req, res) => {
    try {
        const admin = await Admin.findById(req.params.id);
        if (!admin) {
            return res.send(`Admin not Found`);
        } else {
            const { password, _id, __v, ...others } = admin._doc;
            res.status(200).send({ ...others });
        }
    } catch (err) {
        res.status(500).json(err);
    }
};

module.exports = { admin };
