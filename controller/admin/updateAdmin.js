const Admin = require("../../modules/admin");
const CryptoJS = require("crypto-js");
require("dotenv").config();

const updateAdmin = async (req, res) => {
    try {
        const admin = await Admin.findById(req.params.id);
        if (!admin) {
            return res.send(`Admin not Found`);
        } else {
            if (req.body.password) {
                req.body.password = await CryptoJS.AES.encrypt(
                    req.body.password,
                    process.env.PASS_SEC
                ).toString();
            }
            try {
                const updatedAdmin = await Admin.findByIdAndUpdate(
                    req.params.id,
                    {
                        name: req.body.name,
                        email: req.body.email,
                        phoneNo: req.body.phoneNo,
                        password: req.body.password,
                    },
                    { new: true }
                );
                res.status(200).send(updatedAdmin);
            } catch (err) {
                res.status(500).send(err);
            }
        }
    } catch (err) {
        res.status(500).json(err);
    }
};

module.exports = { updateAdmin };
