const CryptoJS = require("crypto-js");
require("dotenv").config();
//const jwt = require("jsonwebtoken");
const Admin = require("../../modules/admin");

const register = async (req, res) => {
    try {
        const newAdmin = new Admin({
            name: req.body.name,
            email: req.body.email,
            phoneNo: req.body.phoneNo,
            password: await CryptoJS.AES.encrypt(
                req.body.name + "@S",
                process.env.PASS_SEC
            ).toString(),
        });

        const admins = await newAdmin.save();
        const { password, _id, __v, isAdmin, ...others } = newAdmin._doc;
        res.status(200).json({ ...others });
    } catch (e) {
        res.status(500).json(e);
    }
};

module.exports = { register };
