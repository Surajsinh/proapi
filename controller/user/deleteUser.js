const User = require("../../modules/user");

const deleteUser = async (req, res) => {
    try {
        await User.findByIdAndDelete(req.params.id);
        res.status(200).send(`User Deleted`);
    } catch (err) {
        res.status(500).json(err);
    }
};

module.exports = { deleteUser };