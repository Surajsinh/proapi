const { response } = require("express");
const jwt = require("jsonwebtoken");
require("dotenv").config();

const verifyToken = (req, res, next) => {
    const authHeaders = req.headers.token;
    if (authHeaders) {
        const token = authHeaders.split(" ")[1];
        jwt.verify(token, process.env.jwt_SEC, (err, user) => {
            if (err) {
                return res.status(403).send(`You are not authenticated user..`);
            } else {
                req.user = user;
                console.log(user);
                next();
            }
        });
    } else {
        return res.status(401).send(`You are not authenticated..`);
    }
};

const verifyTokenAdmin = (req, res, next) => {
    verifyToken(req, res, () => {
        if (req.user.isAdmin) {
            next();
        } else {
            res.status(403).send(`You are not allowed`);
        }
    });
};

const verifyTokenAuth = (req, res, next) => {
    verifyToken(req, res, () => {
        if (req.user.id === req.params.id || res.user.isAdmin) {
            next();
        } else {
            res.status(403).send(`You are not allowed`);
        }
    });
};

module.exports = { verifyToken, verifyTokenAdmin, verifyTokenAuth };
