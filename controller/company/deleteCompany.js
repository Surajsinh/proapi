const Company = require("../../modules/company");

const deleteCompany = async (req, res) => {
    try {
        await Company.findByIdAndDelete(req.params.id);
        res.status(200).send(`Company Deleted`);
    } catch (err) {
        res.status(500).json(err);
    }
};

module.exports = { deleteCompany };