const Company = require("../../modules/company");

const allCompany = async (req, res) => {
    try {
        const company = await Company.find();
        res.status(200).send(company);
    } catch (err) {
        res.status(500).send(err);
    }
};

module.exports = { allCompany };
