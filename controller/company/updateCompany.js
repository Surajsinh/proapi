const Company = require("../../modules/company");
const CryptoJS = require("crypto-js");
require("dotenv").config();

const updateCompany = async (req, res) => {
    try {
        const company = await Company.findById(req.params.id);
        if (!company) {
            return res.send(`Company not Found`);
        } else {
            if (req.body.password) {
                req.body.password = await CryptoJS.AES.encrypt(
                    req.body.password,
                    process.env.PASS_SEC
                ).toString();
            }

            try {
                const updatedCompany = await Company.findByIdAndUpdate(
                    req.params.id,
                    {
                        //$set: req.body,
                        name: req.body.name || company.email,
                        email: req.body.email || company.email,
                        phoneNo: req.body.phoneNo || company.phoneNo,
                        address: {
                            aria_address:
                                req.body.aria_address ||
                                company.address.aria_address,
                            city: req.body.city || company.address.city,
                            state: req.body.state || company.address.state,
                            country:
                                req.body.country || company.address.country,
                            zipcode:
                                req.body.zipcode || company.address.zipcode,
                        },
                        password: req.body.password,
                        about: req.body.about || company.about,
                        category: req.body.category || company.category,
                        acquisitions: {
                            Announced_Date:
                                req.body.announced_date ||
                                company.acquisitions.Announced_Date,
                            Price: req.body.price || company.acquisitions.Price,
                        },
                        funding_received:
                            req.body.funding_received ||
                            company.funding_received,
                        emp_profile: {
                            founder:
                                req.body.founder || company.emp_profile.founder,
                            co_founder:
                                req.body.co_founder ||
                                company.emp_profile.co_founder,
                            ceo: req.body.ceo || company.emp_profile.ceo,
                            cto: req.body.cto || company.emp_profile.cto,
                            vice_president:
                                req.body.vice_president ||
                                company.emp_profile.vice_president,
                        },
                    },
                    { new: true }
                );
                const Companys = await updatedCompany.save();
                res.status(200).send(updatedCompany);
            } catch (err) {
                res.status(500).send(err);
            }
        }
    } catch (err) {
        res.status(500).json(err);
    }
};

module.exports = { updateCompany };
