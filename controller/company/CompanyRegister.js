const CryptoJS = require("crypto-js");
require("dotenv").config();
const Company = require("../../modules/company");

const companyRegister = async (req, res) => {
    try {
        const newCompany = new Company({
            name: req.body.name,
            email: req.body.email,
            phoneNo: req.body.phoneNo,
            address: {
                aria_address: req.body.aria_address,
                city: req.body.city,
                state: req.body.state,
                country: req.body.country,
                zipcode: req.body.zipcode,
            },
            //password: req.body.password,
            password: await CryptoJS.AES.encrypt(
                req.body.password,
                process.env.PASS_SEC
            ).toString(),
            about: req.body.about,
            category: req.body.category,
            acquisitions: {
                Announced_Date: req.body.Announced_Date,
                Price: req.body.Price,
            },
            funding_received:req.body.funding_received,
            emp_profile:{
                founder:req.body.founder,
                co_founder:req.body.co_founder,
                ceo: req.body.ceo,
                cto: req.body.cto,
                vice_president: req.body.vice_president,
            }
        });
        const Companys = await newCompany.save();
        const { password, _id, __v, isAdmin, ...others } = newCompany._doc;
        res.status(200).send({ ...others });
    } catch (e) {
        res.status(500).send(e.message);
    }
};

module.exports = { companyRegister };
