const CryptoJS = require("crypto-js");
require("dotenv").config();
const jwt = require("jsonwebtoken");
const Company = require("../../modules/company");

const companyLogin = async (req, res) => {
    try {
        const company = await Company.findOne({
            email: req.body.email,
        });
        if (!company) {
            return res.status(401).json("Company not found");
        } else {
            const hashPass = CryptoJS.AES.decrypt(
                company.password,
                process.env.PASS_SEC
            );
            const originalPassword = hashPass.toString(CryptoJS.enc.Utf8);
            const inputPassword = req.body.password;

            const accessToken = jwt.sign(
                {
                    id: admin._id,
                    isAdmin: admin.isAdmin,
                },
                process.env.jwt_SEC,
                {
                    expiresIn: "1d",
                }
            );

            if (originalPassword != inputPassword) {
                return res.status(401).json("Wrong Password");
            } else {
                const { password, _id, __v, isAdmin, ...others } = company._doc;
                res.status(200).json({ ...others , accessToken });
            }
        }
    } catch (error) {
        res.send(error.message);
    }
    //res.send("LoginPage");
};

module.exports = { companyLogin };
