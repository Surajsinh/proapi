const mongoose = require("mongoose");

const AdminSchema = new mongoose.Schema(
    {
        isAdmin: { type: Boolean, default: true },
        name: {
            type: String,
            required: true,
        },
        email: {
            type: String,
            required: true,
            unique: true,
            loadClass: true,
        },
        phoneNo: {
            type: Number,
            required: true,
            unique: true,
        },
        password: {
            type: String,
            required: true,
        },
    }
);

const Admin = new mongoose.model("Admin", AdminSchema);

module.exports = Admin;
